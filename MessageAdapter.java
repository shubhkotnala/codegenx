package com.codegenxditu.app;


import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.util.List;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Shubhankar on 9/1/2017.
 */

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder> {

    private List<Messages> mMessageList;
    private FirebaseAuth mAuth;
    private DatabaseReference mProfileImageRef;
    //  private final String image=null;


    public MessageAdapter(List<Messages> mMessageList){
        this.mMessageList= mMessageList;
    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.timeline_single_layout, parent, false);

        return new MessageViewHolder(v);
    }

    public class MessageViewHolder  extends RecyclerView.ViewHolder{

    private TextView messageText;
    private TextView messageDate;
    private CircleImageView profileImage;
    private TextView messageTitle;


    public MessageViewHolder(View itemView) {
        super(itemView);

      //  messageText = (TextView) itemView.findViewById(R.id.message_text);
       // profileImage = (CircleImageView) itemView.findViewById(R.id.message_profile_layout);
  //      messageTitle =(TextView)itemView.findViewById(R.id.message_title);
//        messageDate= (TextView) itemView.findViewById(R.id.message_date);


    }
}

    @Override
    public void onBindViewHolder(MessageAdapter.MessageViewHolder holder, int position) {
        mAuth = FirebaseAuth.getInstance();
        String current_user_id = mAuth.getCurrentUser().getUid();
        Messages c= mMessageList.get(position);
        final String message_title = c.getTitle();

        mProfileImageRef= FirebaseDatabase.getInstance().getReference().child("Users");

      //  holder.messageText.setText(c.getMessage());
    }

    @Override
    public int getItemCount() {
        return mMessageList.size();
    }

}
