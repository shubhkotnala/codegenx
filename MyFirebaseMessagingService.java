package com.codegenxditu.app;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import static android.content.ContentValues.TAG;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...
        Map<String, String> data = remoteMessage.getData();
        String notification_title=remoteMessage.getNotification().getTitle();

        String notification_body=remoteMessage.getNotification().getBody();
        String click_Action = remoteMessage.getData().get("click");
        String click = remoteMessage.getNotification().getClickAction();
       // String from_user_id = remoteMessage.getData().get("from_user_id");

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(MyFirebaseMessagingService.this)
                        .setSmallIcon(R.drawable.app_icon)
                        .setContentTitle(notification_title)
                        .setContentText(notification_body);

     //   if(data.get("click_action")!=null){
     //       ClickActionHelper.startActivity(data.get("click_action"), null, this);
     //   }

        Class cls=null;
        try {
            cls = Class.forName(click_Action);
        }catch(ClassNotFoundException e){
            //means you made a wrong input in firebase console
        }
        Intent i = new Intent(this, cls);
      //  this.startActivity(i);

 // Intent resultIntent = new Intent(MyFirebaseMessagingService.this,TestActivity.class);
       // resultIntent.putExtra("user_id",from_user_id);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        i,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        //startActivity(resultIntent);

        mBuilder.setContentIntent(resultPendingIntent);

        // Sets an ID for the notification
        int mNotificationId = (int)System.currentTimeMillis();
// Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
// Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());}
}
