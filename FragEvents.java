package com.codegenxditu.app;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Shubhankar on 9/1/2017.
 */

public class FragEvents extends android.support.v4.app.Fragment {

    private RecyclerView mMemberList;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private View mView;
    private GridLayoutManager mGridLayoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments

        mView = inflater.inflate(R.layout.team_fragment, container, false);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("events");

        mMemberList = (RecyclerView) mView.findViewById(R.id.mem_list);
        mGridLayoutManager = new GridLayoutManager(getActivity(),1);
        mMemberList.setHasFixedSize(true);
        mMemberList.setLayoutManager(mGridLayoutManager);
        return mView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Team");
    }

    @Override
    public void onStart() {
        super.onStart();

        final FirebaseRecyclerAdapter<ListItem, FragEvents.EventsViewHolder> firebaseRecyclerAdapter= new FirebaseRecyclerAdapter<ListItem, FragEvents.EventsViewHolder>(
                ListItem.class,
                R.layout.event_single_layout,
                FragEvents.EventsViewHolder.class,
                mDatabase
        ) {
            @Override
            protected void populateViewHolder(final FragEvents.EventsViewHolder viewHolder, ListItem item, int i) {
                final String list_user_id=getRef(i).getKey();
                mDatabase.child(list_user_id).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String date = dataSnapshot.child("date").getValue().toString();
                        String title = dataSnapshot.child("name").getValue().toString();
                        String image = dataSnapshot.child("image").getValue().toString();
                        String description = dataSnapshot.child("body").getValue().toString();

                        viewHolder.setName(title);
                        viewHolder.setDAte(date);
                        viewHolder.setImage(image,getContext());

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });



            }
        };

        mMemberList.setAdapter(firebaseRecyclerAdapter);


    }

    public static class EventsViewHolder extends RecyclerView.ViewHolder{

        View mView;

        public EventsViewHolder(View itemView){
            super(itemView);
            mView = itemView;
        }

        public void setName(String name){

            TextView mName = (TextView) mView.findViewById(R.id.eventTitle);
            mName.setText(name);

        }

        public void setDAte(String Date){

            TextView mDate = (TextView) mView.findViewById(R.id.eventDate);
            mDate.setText(Date);

        }

        public void setImage(final String imageUrl, final Context ctx){

            final ImageView mImage = (ImageView) mView.findViewById(R.id.eventLogo);
            final Context c = ctx;
            final String img = imageUrl;
            Picasso.with(ctx).load(imageUrl).networkPolicy(NetworkPolicy.OFFLINE)
                    .placeholder(R.drawable.logo)
                    .into(mImage, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Picasso.with(ctx).load(imageUrl)
                                    .placeholder(R.drawable.logo).into(mImage);
                        }
                    });
        }

    }



}
