package com.codegenxditu.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

/**
 * Created by Shubhankar on 9/2/2017.
 */

public class StartActivity extends AppCompatActivity {
    private Button mRegBtn;
    private Button mLoginBtn;
    private Button mPhoneAuthBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
getSupportActionBar().setElevation(0);
getSupportActionBar().setTitle("");
        mRegBtn=(Button) findViewById(R.id.start_reg_button);
        mLoginBtn=(Button) findViewById(R.id.start_login_btn);
        mPhoneAuthBtn = (Button) findViewById(R.id.start_phone_auth_button);
    //    mPhoneAuthBtn.setVisibility(View.GONE);


        mRegBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent reg_intent= new Intent(StartActivity.this, RegisterActivity.class);
                startActivity(reg_intent);
            }
        });

        mLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent login_intetn= new Intent(StartActivity.this, LoginActivity.class);
                startActivity(  login_intetn);
            }
        });

        mPhoneAuthBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent phone_intent = new Intent(StartActivity.this, PhoneAuthActivity.class);
                startActivity(phone_intent);
            }
        });
    }
}
