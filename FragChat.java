package com.codegenxditu.app;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by Shubhankar on 9/6/2017.
 */

public class FragChat extends android.support.v4.app.Fragment {

    private Button mSubmitBtn;
    private TextInputLayout mSub;
    private TextInputLayout mPhone;
    private TextInputLayout mBody;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        View v = inflater.inflate(R.layout.frag_chat, container, false);
        mSub = (TextInputLayout) v.findViewById(R.id.chat_sub);
        mSubmitBtn = (Button) v.findViewById(R.id.chat_btn_submit);
        mPhone=(TextInputLayout) v.findViewById(R.id.chat_phone);
        mBody = (TextInputLayout) v.findViewById(R.id.chat_body);
        return v;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("CodeGenX");

        mSubmitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("message/rfc822");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"skshubhankar@gmail.com"});
                intent.putExtra(Intent.EXTRA_SUBJECT, mSub.getEditText().getText().toString());
                intent.putExtra(Intent.EXTRA_TEXT,mPhone.getEditText().getText().toString()+ "\n\n" + mBody.getEditText().getText().toString().trim());
                try {
                    startActivity(Intent.createChooser(intent, "Choose an Email client :"));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getContext(), "Message not sent! Pls install an E-mail client and try again.", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
}
