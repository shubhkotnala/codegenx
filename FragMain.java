package com.codegenxditu.app;

import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.*;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Shubhankar on 9/1/2017.
 */
public class FragMain extends android.support.v4.app.Fragment {

    String strDate;
    SimpleDateFormat sdf;
    Calendar c;
    TextView quoteToday;
    ImageView quoteShare;
    String quote;
    List<ListItem> list;
    ItemTouchHelper.SimpleCallback simpleItemTouchCallback;
    private LinearLayoutManager linearLayoutManager;
    private TextView timelineTitle;
    private DatabaseReference databaseReference;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private RecyclerAdapter recyclerAdapter;
    private RecyclerView mFriendsList;
    private DatabaseReference mFriendsDatabase;
    private DatabaseReference mUsersDatabase;

    private String mCurrent_user_id;

    private View mMainView;

    public FragMain() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        View v = inflater.inflate(R.layout.frag_main, container, false);

        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        quoteToday = (TextView) v.findViewById(R.id.quote_text);
        quoteToday.setText("Hello world test");
        timelineTitle = (TextView) v.findViewById(R.id.userName);
        quoteShare = (ImageView) v.findViewById(R.id.quote_shareBtn);
        if (mAuth.getCurrentUser() != null) {
            databaseReference = FirebaseDatabase.getInstance().getReference().child("Users")
                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid());
            databaseReference.keepSynced(true);
            // startActivity(new Intent(getContext(), StartActivity.class));
        }
        mFriendsList = (RecyclerView) v.findViewById(R.id.timeline_list);
        mAuth = FirebaseAuth.getInstance();

        mCurrent_user_id = mAuth.getCurrentUser().getUid();

        mFriendsDatabase = FirebaseDatabase.getInstance().getReference().child("timeline");
        mFriendsDatabase.keepSynced(true);
        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("timeline");
        mUsersDatabase.keepSynced(true);

        mFriendsList.setHasFixedSize(false);
        mFriendsList.setLayoutManager(linearLayoutManager);
        //  mFriendsList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, true));

        simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                    RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                Toast.makeText(getContext(), "Item removed", Toast.LENGTH_SHORT).show();
                //Remove swiped item from list and notify the RecyclerView
                int position = viewHolder.getAdapterPosition();
                list.remove(position);
                recyclerAdapter.notifyItemRemoved(position);
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(mFriendsList);

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Toast.makeText(getContext(), "View Created", Toast.LENGTH_SHORT).show();
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("CodeGenX");

        if (mAuth.getCurrentUser() != null) {
            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String name = dataSnapshot.child("name").getValue().toString();
                    //  timelineTitle.setText("Welcome " + name);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        c = Calendar.getInstance();
        sdf = new SimpleDateFormat("dd MMMM");
        dayFact();

        quoteShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sharingIntent = new Intent("android.intent.action.SEND");
                sharingIntent.setType("text/plain");
                String sharingBody = quote + "\n\nQuote of the day brought to you by CodeGenX";
                sharingIntent.putExtra("android.intent.extra.SUBJECT", "Quote of the Day");
                sharingIntent.putExtra("android.intent.extra.TEXT", sharingBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via..."));
            }
        });
        strDate = sdf.format(c.getTime());
        final FirebaseRecyclerAdapter<ListItem, NewsViewHolder> newsRecyclerViewAdapter= new FirebaseRecyclerAdapter<ListItem, NewsViewHolder>(
                ListItem.class,
                R.layout.timeline_single_layout,
                NewsViewHolder.class,
                mFriendsDatabase
        ) {
            @Override
            protected void populateViewHolder(final NewsViewHolder viewHolder, ListItem item, int i) {
                final String list_user_id=getRef(i).getKey();
                mFriendsDatabase.child(list_user_id).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String title = dataSnapshot.child("name").getValue().toString();
                        String body = dataSnapshot.child("text").getValue().toString();
                        String author = dataSnapshot.child("time").getValue().toString();
                        String image = dataSnapshot.child("image").getValue().toString();

                        viewHolder.setAuthor(author);
                        viewHolder.setBody(body);
                        viewHolder.setTitle(title);
                        viewHolder.setImage(image);

                        viewHolder.mView.setOnClickListener( v -> {

                            Intent intent = new Intent(getContext(),NewsActivity.class);
                            intent.putExtra("body",body);
                            intent.putExtra("title",title);
                            intent.putExtra("image",image);
                            startActivity(intent);

                        });

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });



            }
        };

        mFriendsList.setAdapter(newsRecyclerViewAdapter);
    }

    public static class NewsViewHolder extends RecyclerView.ViewHolder {

        View mView;
        CircleImageView userImageView;
        public NewsViewHolder(View itemView){
            super(itemView);
            mView=itemView;
        }

        public void setTitle(String title){
            TextView txtTitle = itemView.findViewById(R.id.feed_default_title);
            txtTitle.setText(title);
        }
        public void setBody(String body){
            TextView txtTitle = itemView.findViewById(R.id.feed_default_body);
            txtTitle.setText(body);
        }
        public void setAuthor(String author){
            TextView txtTitle = itemView.findViewById(R.id.feed_default_author);
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            String dateString = formatter.format(new Date(Long.parseLong(author)));
            txtTitle.setText(dateString);
        }
        public void setImage(final String image){
            userImageView = (CircleImageView) itemView.findViewById(R.id.feed_default_image);

            if(!image.equals("default")){
                Picasso.with(userImageView.getContext()).load(image)
                        .networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.logo)
                        .into(userImageView, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                                Picasso.with(userImageView.getContext()).load(image)
                                        .placeholder(R.drawable.logo)
                                        .error(R.drawable.logo)
                                        .into(userImageView);
                            }
                        });
            }


        }


    }

    public void dayFact() {

        InputStream is = this.getResources().openRawResource(R.raw.quote_today);
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        //Toast.makeText(getContext(),"after reader",Toast.LENGTH_SHORT).show();
        c = Calendar.getInstance();
        sdf = new SimpleDateFormat("dd MMMM");
        strDate = sdf.format(c.getTime());
        try {
            String in, word, secondWord, thirdWord;
            //   Toast.makeText(getContext(),"3",Toast.LENGTH_SHORT).show();
            while ((in = reader.readLine()) != null) {
                //      Toast.makeText(getContext(),"4",Toast.LENGTH_SHORT).show();
                String[] RowData = in.split("~");
                word = RowData[0];
                secondWord = RowData[1];
                //    Toast.makeText(getContext(),"initialization",Toast.LENGTH_SHORT).show();
                if (strDate.equals(word) || strDate.equals(secondWord)) {
                    //       Toast.makeText(getContext(),"5",Toast.LENGTH_SHORT).show();
                    thirdWord = RowData[2];
                    quote = thirdWord;
                    quoteToday.setText(thirdWord);
                    break;
                } else {
                    quoteToday.setText(
                            "Error getting Quote. Let us know about this issue via mail.\ncodegenxditu@gmail.com");
                }
            }
            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

}
