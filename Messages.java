package com.codegenxditu.app;

/**
 * Created by Shubhankar on 9/1/2017.
 */

public class Messages {

    private String message;
    private String title;
    private long time;


    public Messages(String message, long time){
        this.message=message;
        this.time=time;


    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }



    public String getTitle() {
        return title;
    }

    public void setTitle(String from) {
        this.title = from;
    }

    public Messages(){

    }

}
