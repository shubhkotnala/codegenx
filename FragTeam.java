package com.codegenxditu.app;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Shubhankar on 9/1/2017.
 */

public class FragTeam extends android.support.v4.app.Fragment {

    private RecyclerView mMemberList;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private View mView;
    private GridLayoutManager mGridLayoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments

        mView = inflater.inflate(R.layout.team_fragment, container, false);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("members");

        mMemberList = (RecyclerView) mView.findViewById(R.id.mem_list);
        mGridLayoutManager = new GridLayoutManager(getActivity(),2);
        mMemberList.setHasFixedSize(true);
        mMemberList.setLayoutManager(mGridLayoutManager);


            return mView;
        }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Team");
    }

    @Override
    public void onStart() {
        super.onStart();

        final FirebaseRecyclerAdapter<ListItem, MembersViewHolder> firebaseRecyclerAdapter= new FirebaseRecyclerAdapter<ListItem, MembersViewHolder>(
                ListItem.class,
                R.layout.member_single_layout,
                MembersViewHolder.class,
                mDatabase
        ) {
            @Override
            protected void populateViewHolder(final MembersViewHolder viewHolder, ListItem item, int i) {
                final String list_user_id=getRef(i).getKey();
                mDatabase.child(list_user_id).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String name = dataSnapshot.child("name").getValue().toString();
                        String desig = dataSnapshot.child("desig").getValue().toString();
                        String image = dataSnapshot.child("image").getValue().toString();

                        viewHolder.setName(name);
                        viewHolder.setDesig(desig);
                        viewHolder.setImage(image,getContext());

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });



            }
        };

        mMemberList.setAdapter(firebaseRecyclerAdapter);


    }

    public static class MembersViewHolder extends RecyclerView.ViewHolder{

        View mView;

        public MembersViewHolder(View itemView){
                super(itemView);
            mView = itemView;
        }

        public void setName(String name){

            TextView mName = (TextView) mView.findViewById(R.id.mem_single_name);
            mName.setText(name);

        }

        public void setDesig(String Desig){

            TextView mDesig = (TextView) mView.findViewById(R.id.mem_single_post);
            mDesig.setText(Desig);

        }

        public void setImage(final String imageUrl, final Context ctx){

            final CircleImageView mImage = (CircleImageView) mView.findViewById(R.id.mem_single_image);
                final Context c = ctx;
                final String img = imageUrl;
            Picasso.with(ctx).load(imageUrl).networkPolicy(NetworkPolicy.OFFLINE)
                    .placeholder(R.drawable.logo)
                    .into(mImage, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Picasso.with(ctx).load(imageUrl)
                                    .placeholder(R.drawable.logo).into(mImage);
                        }
                    });
        }

    }



}
