package com.codegenxditu.app;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.MenuInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabaseRef;
    private CircleImageView mNavUserImage;
    private TextView mNavUserName;
    private TextView mNavUserStatus;
    private ImageView mNavEditBtn;
    private String status = "default";
    private String mCurrentUser=null;
    private String adminName="User";
    private NavigationView mNavView;

    @Override
    protected void onCreate(Bundle savedInstanceState) throws NullPointerException {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("CodeGenX");
        checkIntent(getIntent());
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        mNavView = (NavigationView) findViewById(R.id.nav_view) ;

        mNavUserImage = (CircleImageView)mNavView.getHeaderView(0).findViewById(R.id.nav_image);
        mNavUserName = (TextView)mNavView.getHeaderView(0).findViewById(R.id.nav_name);
        mNavUserStatus = (TextView)mNavView.getHeaderView(0).findViewById(R.id.nav_status);
        mNavEditBtn = (ImageView) mNavView.getHeaderView(0).findViewById(R.id.nav_edit_btn);

        mNavEditBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent statusIntent = new Intent(MainActivity.this,StatusActivity.class);
                statusIntent.putExtra("status_value",status);
                startActivity(statusIntent);
                recreate();
            }
        });

        mNavUserImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SettingsActivity.class));
            }
        });

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser == null){
            sendToStart();
        }
        else{

            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    Fragment fragment = new FragChat();
                    ft.replace(R.id.content_frame, fragment);
                    ft.commit();
                }
            });

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment fragment = new FragMain();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
            FirebaseMessaging.getInstance().subscribeToTopic("pushNotifications");

            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
            try {
                mCurrentUser = mAuth.getCurrentUser().getUid();
            } catch (NullPointerException n){
                n.printStackTrace();
            }

            mDatabaseRef = FirebaseDatabase.getInstance().getReference().child("Users").child(mCurrentUser);
            mDatabaseRef.keepSynced(true);
            mDatabaseRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String name = dataSnapshot.child("name").getValue().toString();
                    String thumb_image = dataSnapshot.child("image").getValue().toString();
                    adminName = name;
                    if(name.equals("CodeGenX")){

                        invalidateOptionsMenu();
                    }

                    mNavUserName.setText(name);
                    status = dataSnapshot.child("status").getValue().toString();
                    mNavUserStatus.setText(dataSnapshot.child("status").getValue().toString());

                    if(!thumb_image.equals("default")){
                        Picasso.with(MainActivity.this).load(thumb_image).networkPolicy(NetworkPolicy.OFFLINE)
                                .placeholder(R.drawable.default_avatar).into(mNavUserImage, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                                Picasso.with(MainActivity.this).load(thumb_image)
                                        .placeholder(R.drawable.default_avatar).into(mNavUserImage);
                            }
                        });
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent startIntent = new Intent(getApplicationContext(), MyService.class);
        startIntent.setAction("C.ACTION_START_SERVICE");
        Log.d("Service","Started");
        startService(startIntent);
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser == null){
            sendToStart();
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        MenuInflater inflater = getMenuInflater();

        if (adminName.equals("CodeGenX")) {
            inflater.inflate(R.menu.main2, menu);
        }
        else {
            inflater.inflate(R.menu.main, menu);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       switch (id){
           case R.id.action_logout:
               FirebaseAuth.getInstance().signOut();
               sendToStart();
               break;
           case R.id.action_account_settings:
               startActivity(new Intent(MainActivity.this,SettingsActivity.class));
               break;
           case R.id.action_adminPanel:
               startActivity(new Intent(MainActivity.this,AdminConsoleActivity.class));
               break;
             // Toast.makeText(getApplicationContext(),"Admin Console Clicked",Toast.LENGTH_SHORT).show();
       }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        displaySelectedScreen(item.getItemId());
        return true;
    }
    private void displaySelectedScreen(int itemId) {

        //creating fragment object
        Fragment fragment = null;

        //initializing the fragment object which is selected
        switch (itemId) {
            case R.id.nav_sir:
                fragment = new FragDirector();
                break;
            case R.id.nav_events:
                fragment = new FragEvents();
                break;
            case R.id.nav_home:
                fragment = new FragMain();
                break;
            case R.id.nav_upcomingEvents:
                fragment = new FragUpEvents();
                break;
            case R.id.nav_team:
                fragment = new FragTeam();
                break;
            case R.id.nav_news:
                fragment = new FragUpEvents();
                break;
            default:
                fragment = new FragMain();
                break;
        }

        //replacing the fragment
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    //Extra for click_action
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        checkIntent(intent);
    }

    public void checkIntent(Intent intent) {
        if (intent.hasExtra("click")) {
            Toast.makeText(this,"Intent has extra",Toast.LENGTH_SHORT).show();
            ClickActionHelper.startActivity(intent.getStringExtra("click"), intent.getExtras(), this);
        }
    }

    private void sendToStart() {
        Intent startIntent= new Intent(MainActivity.this,StartActivity.class);
        startActivity(startIntent);
        finish();
    }

}
