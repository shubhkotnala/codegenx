package com.codegenxditu.app;

import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

public class NewsActivity extends AppCompatActivity {

    private String title;
    private String body;
    private String image;

    private Toolbar mToolbar;
    private TextView mtitle;
    private TextView mBody;
    private ImageView mImage;

    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        Window w = getWindow(); // in Activity's onCreate() for instance
      //  w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        mToolbar = (Toolbar) findViewById(R.id.anim_toolbar);
      //  setSupportActionBar(mToolbar);
       // getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fab = (FloatingActionButton) findViewById(R.id.fab);

        final CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {

                   collapsingToolbarLayout.setTitle("CodeGenX");
                    isShow = true;
                } else if(isShow) {
                   collapsingToolbarLayout.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });

        title = getIntent().getStringExtra("title");
        body = getIntent().getStringExtra("body");
        image = getIntent().getStringExtra("image");

        mtitle = (TextView) findViewById(R.id.news_title_content);
        mBody = (TextView) findViewById(R.id.news_body_content);
        mImage = (ImageView) findViewById(R.id.news_image);
       // getSupportActionBar().setTitle(title+"very long title goes here");
       mtitle.setText(title);
        mBody.setText(body);

        fab.setOnClickListener(v -> {
            Intent sharingIntent = new Intent("android.intent.action.SEND");
            sharingIntent.setType("text/plain");
            String sharingBody = title + "\n\n"  + body + "\n\n\nFor more such news download the CodeGenX now!";
            sharingIntent.putExtra("android.intent.extra.SUBJECT", "Check out this Article");
            sharingIntent.putExtra("android.intent.extra.TEXT", sharingBody);
            startActivity(Intent.createChooser(sharingIntent, "Share via..."));
        });
        Log.d("Image URI",image);

        if(!image.equals("default")){

            Picasso.with(getApplicationContext()).load(image).networkPolicy(NetworkPolicy.OFFLINE)
                    .placeholder(R.drawable.logo).into(mImage, new Callback() {
                @Override
                public void onSuccess() {
                    //NAACHO, LOAD HO GAYA OFFLINE :D
                }

                @Override
                public void onError() {
                    Picasso.with(getApplicationContext()).load(image)
                            .placeholder(R.drawable.logo).into(mImage);
                }
            });


        }

    }
}
