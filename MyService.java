package com.codegenxditu.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

/**
 * Created by Shubhankar on 9/4/2017.
 */

public class MyService extends Service {
    static final int NOTIFICATION_ID = 543;

    public static boolean isServiceRunning = false;

    @Override
    public void onCreate() {
        super.onCreate();
            startServiceWithNotification();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent != null && intent.getAction().equals("C.ACTION_START_SERVICE")){
            startServiceWithNotification();
        }
        else {
            stopMyService();
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        isServiceRunning = false;
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    void startServiceWithNotification(){
        if(isServiceRunning) return;
        isServiceRunning = true;

        Intent notificationIntent = new Intent(getApplicationContext(),MainActivity.class);
        notificationIntent.setAction("C.ACTION_MAIN");
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent contentPendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

        Notification notification = new NotificationCompat.Builder(this)
              //  .setContentTitle("CodeGenX")
              //  .setTicker("COdeGenX")
              //  .setContentText("Notification Service")
              //  .setSmallIcon(R.mipmap.ic_launcher);
                // .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setContentIntent(contentPendingIntent)
                .setOngoing(true)
                .build();
        notification.flags = notification.flags | Notification.FLAG_NO_CLEAR;
        startForeground(NOTIFICATION_ID, notification);

      //  startService(new Intent(this, MyFirebaseMessagingService.class));
      //  startService(new Intent(this, MyFirebaseInstanceIDService.class));

    }
    void stopMyService(){
        stopForeground(true);
        stopSelf();
        isServiceRunning = false;
    }
}
