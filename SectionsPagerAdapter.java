package com.codegenxditu.app;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by Shubhankar on 9/2/2017.
 */

public class SectionsPagerAdapter extends FragmentPagerAdapter {


    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
               NewsfeedFragment newsfeedFragment = new NewsfeedFragment();
               return newsfeedFragment;
            case 1:
                NotificationFragment notificationFragment = new NotificationFragment();
                return notificationFragment;
            case 2:
                EventsFragment eventsFragment = new EventsFragment();
                return eventsFragment;
            case 3:
                MemberFragment memberFragment = new MemberFragment();
                return memberFragment;
            default:
                return null;
        }


    }

    @Override
    public int getCount() {
        return 4;
    }

    public CharSequence getPageTitle(int position){

        switch (position){
            case 0:
                return "NEWSFEED";
            case 1:
                return "NOTIFICATIONS";
            case 2:
                return "EVENTS";
            case 3:
                return "NEW MEMBER";
            default:
                return null;
        }

    }
}
