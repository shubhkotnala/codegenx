package com.codegenxditu.app;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;
import java.util.Map;

public class MemberFragment extends Fragment {

    private TextInputLayout mName;
    private TextInputLayout mDesig;
    private TextInputLayout mImageUrl;
    private TextInputLayout mClickAction;
    private DatabaseReference mDatabase;
    private FirebaseUser mUser;
    private ProgressDialog mProgress;
    private Button mPushBtn;

    public MemberFragment() {
        //Required empty constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.members_fragment, container, false);

        mName = (TextInputLayout) v.findViewById(R.id.mem_name);
        mDesig = (TextInputLayout) v.findViewById(R.id.mem_desig);
        mImageUrl = (TextInputLayout) v.findViewById(R.id.mem_image);
        mPushBtn = (Button) v.findViewById(R.id.mem_save_btn);
        //  mClickAction = (TextInputLayout) v.findViewById();

        //   mProgress = new ProgressDialog(getContext());

        mUser = FirebaseAuth.getInstance().getCurrentUser();
        String uid = mUser.getUid();
        mDatabase= FirebaseDatabase.getInstance().getReference().child("members").push();

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        mPushBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) throws NullPointerException {
                String memName = mName.getEditText().getText().toString();
                String memDesig = mDesig.getEditText().getText().toString();
                String memImageUrl = mImageUrl.getEditText().getText().toString();
                if (TextUtils.isEmpty(memImageUrl)){
                    memImageUrl = "default";
                }
                if(!TextUtils.isEmpty(memDesig)||!TextUtils.isEmpty(memName)){
                    Map messageMap = new HashMap();
                    messageMap.put("name",memName);
                    messageMap.put("desig",memDesig);
                    messageMap.put("image",memImageUrl);
                    mDatabase.updateChildren(messageMap, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                            if(databaseError != null){
                                Log.d("Push Unsuccessful",databaseError.getDetails());
                            }
                            else{
                                Toast.makeText(getContext(),"Push Successful",Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
                if(TextUtils.isEmpty(memDesig)){
                    mDesig.setError("Body Cannot be empty");
                }
                if(TextUtils.isEmpty(memName)){
                    mName.setError("Title cannot be empty");
                }
            }
        });

    }

}
