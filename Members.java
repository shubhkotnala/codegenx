package com.codegenxditu.app;

public class Members {

    public String name;
    public String desig;
    public String image;

    public Members(String name, String desig, String image) {
        this.name = name;
        this.desig = desig;
        this.image = image;
    }

    public Members() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesig() {
        return desig;
    }

    public void setDesig(String desig) {
        this.desig = desig;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
