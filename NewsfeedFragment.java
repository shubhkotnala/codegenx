package com.codegenxditu.app;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Shubhankar on 9/2/2017.
 */

public class NewsfeedFragment extends Fragment {

    private TextInputLayout mTitle;
    private TextInputLayout mBody;
    private TextInputLayout mImageUrl;
    private TextInputLayout mClickAction;
    private DatabaseReference mDatabase;
    private FirebaseUser mUser;
    private ProgressDialog mProgress;
    private Button mPushBtn;

    public NewsfeedFragment() {
        //Required empty constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.newsfeed_fragment, container, false);

        mTitle = (TextInputLayout) v.findViewById(R.id.news_title);
        mBody = (TextInputLayout) v.findViewById(R.id.news_body);
        mImageUrl = (TextInputLayout) v.findViewById(R.id.news_link);
        mPushBtn = (Button) v.findViewById(R.id.news_send_btn);
        //  mClickAction = (TextInputLayout) v.findViewById();

        //   mProgress = new ProgressDialog(getContext());

        mUser = FirebaseAuth.getInstance().getCurrentUser();
        String uid = mUser.getUid();
        mDatabase= FirebaseDatabase.getInstance().getReference().child("timeline").push();

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        mPushBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) throws NullPointerException {
                String newsTitle = mTitle.getEditText().getText().toString();
                String newsBody = mBody.getEditText().getText().toString();
                String newsImageUrl = mImageUrl.getEditText().getText().toString();
                if (TextUtils.isEmpty(newsImageUrl)){
                    newsImageUrl = "default";
                }
                if(!TextUtils.isEmpty(newsBody)||!TextUtils.isEmpty(newsTitle)){
                    Map messageMap = new HashMap();
                    messageMap.put("name",newsTitle);
                    messageMap.put("text",newsBody);
                    messageMap.put("time", ServerValue.TIMESTAMP);
                    messageMap.put("image",newsImageUrl);
                    mDatabase.updateChildren(messageMap, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                            if(databaseError != null){
                                Log.d("Push Unsuccessful",databaseError.getDetails());
                            }
                            else{
                                Toast.makeText(getContext(),"Push Successful",Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
                if(TextUtils.isEmpty(newsBody)){
                    mBody.setError("Body Cannot be empty");
                }
                if(TextUtils.isEmpty(newsTitle)){
                    mTitle.setError("Title cannot be empty");
                }
            }
        });

    }
}
