package com.codegenxditu.app;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;
import java.util.Map;

public class EventsFragment extends Fragment {

    private TextInputLayout mTitle;
    private TextInputLayout mBody;
    private TextInputLayout mImageUrl;
    private TextInputLayout mDate;
    private TextInputLayout mClickAction;
    private DatabaseReference mDatabase;
    private FirebaseUser mUser;
    private ProgressDialog mProgress;
    private Button mPushBtn;

    public EventsFragment() {
        //Required empty constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.events_fragment, container, false);

        mTitle = (TextInputLayout) v.findViewById(R.id.event_title);
        mBody = (TextInputLayout) v.findViewById(R.id.event_body);
        mImageUrl = (TextInputLayout) v.findViewById(R.id.event_link);
        mPushBtn = (Button) v.findViewById(R.id.event_send_btn);
        mDate = (TextInputLayout) v.findViewById(R.id.event_date);
        //  mClickAction = (TextInputLayout) v.findViewById();

        //   mProgress = new ProgressDialog(getContext());

        mUser = FirebaseAuth.getInstance().getCurrentUser();
        String uid = mUser.getUid();
        mDatabase= FirebaseDatabase.getInstance().getReference().child("events").push();

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        mPushBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) throws NullPointerException {
                String eventTitle = mTitle.getEditText().getText().toString();
                String eventBody = mBody.getEditText().getText().toString();
                String eventImageUrl = mImageUrl.getEditText().getText().toString();
                String eventDate = mDate.getEditText().getText().toString();
                if (TextUtils.isEmpty(eventImageUrl)){
                    eventImageUrl = "default";
                }
                if(!TextUtils.isEmpty(eventBody)||!TextUtils.isEmpty(eventTitle)){
                    Map messageMap = new HashMap();
                    messageMap.put("name",eventTitle);
                    messageMap.put("body",eventBody);
                    messageMap.put("timestamp", ServerValue.TIMESTAMP);
                    messageMap.put("date",eventDate);
                    messageMap.put("image",eventImageUrl);
                    mDatabase.updateChildren(messageMap, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                            if(databaseError != null){
                                Log.d("Push Unsuccessful",databaseError.getDetails());
                            }
                            else{
                                Toast.makeText(getContext(),"Push Successful",Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
                if(TextUtils.isEmpty(eventBody)){
                    mBody.setError("Body Cannot be empty");
                }
                if(TextUtils.isEmpty(eventTitle)){
                    mTitle.setError("Title cannot be empty");
                }
            }
        });

    }

}
