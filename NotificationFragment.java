package com.codegenxditu.app;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Shubhankar on 9/2/2017.
 */

public class NotificationFragment extends Fragment {

    private TextInputLayout mTitle;
    private TextInputLayout mBody;
    private TextInputLayout mClickAction;
    private DatabaseReference mDatabase;
    private FirebaseUser mUser;
    private ProgressDialog mProgress;
    private Button mPushBtn;

    public NotificationFragment() {
        //Required empty constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.notification_fragment, container, false);

        mTitle = (TextInputLayout) v.findViewById(R.id.notification_name);
        mBody = (TextInputLayout) v.findViewById(R.id.notification_data);
        mPushBtn = (Button) v.findViewById(R.id.notification_send);
      //  mClickAction = (TextInputLayout) v.findViewById();

     //   mProgress = new ProgressDialog(getContext());

        mUser = FirebaseAuth.getInstance().getCurrentUser();
        String uid = mUser.getUid();
        mDatabase= FirebaseDatabase.getInstance().getReference().child("messages").push();

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        mPushBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) throws NullPointerException {
                String notificationTitle = mTitle.getEditText().getText().toString();
                String notificationBody = mBody.getEditText().getText().toString();
                if(!TextUtils.isEmpty(notificationBody)||!TextUtils.isEmpty(notificationTitle)){
                    Map messageMap = new HashMap();
                    messageMap.put("name",notificationTitle);
                    messageMap.put("text",notificationBody);
                    messageMap.put("click","com.codegenxditu.codegenx.TestActivity");
                    messageMap.put("click_action","com.codegenxditu.codegenx.TestActivity");

                    mDatabase.updateChildren(messageMap, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                            if(databaseError != null){

                            }
                            else{
                                Toast.makeText(getContext(),"Push Successful",Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
                else{
                    Toast.makeText(getContext(),"Text Fields Cannot be empty",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
