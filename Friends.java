package com.codegenxditu.app;

/**
 * Created by Shubhankar on 9/4/2017.
 */

public class Friends {
    public String date;
    public String name;
    public String text;

    public Friends(){

    }
    public Friends(String date) {
        this.date=date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
